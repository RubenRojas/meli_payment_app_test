package cl.ruben.paymentapp.domain.entities


data class BankEntity(
    val name: String,
    val thumbnail: String,
    val id: String,
)

data class InstallmentRequestEntity(
    val payment_id: String,
    val amount: Int,
    val issuer_id: Int,
)

data class PaymentEntity(
    val bank: BankEntity?,
    val payment: PaymentMethodEntity?,
    val installment: InstallmentOptionEntity?,
    val amount: Int?,
    val hasDisplayed: Boolean = false
)

data class PaymentMethodEntity(
    val name: String,
    val thumbnail: String,
    val id: String,
    val minAmount: Long,
    val maxAmount: Long,
    val paymentTypeId: String,
)

data class InstallmentEntity(
    val paymentMethodId: String,
    val paymentTypeId: String,
    val issuer: BankEntity,
    val options: List<InstallmentOptionEntity>
)

data class InstallmentOptionEntity(
    val id: String,
    val message: String,
    val installments: Long, // cuotas
    val installmentAmount: Double, // cantidad_cuotas
    val totalAmount: Double,  // montoTotal
    val installment_rate: Double,
    val labels: List<String>,
)
