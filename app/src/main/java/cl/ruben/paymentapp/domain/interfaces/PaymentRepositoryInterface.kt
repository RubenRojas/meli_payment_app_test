package cl.ruben.paymentapp.domain.interfaces

import cl.ruben.paymentapp.domain.entities.*

interface PaymentRepositoryInterface {
    suspend fun fetchPaymentMethods(): List<PaymentMethodEntity>
    suspend fun fetchBankList(paymentId: String): List<BankEntity>
    suspend fun fetchInstallments(requestEntity: InstallmentRequestEntity): List<InstallmentEntity>
    suspend fun createLocal(data: PaymentEntity): Int
    suspend fun updateProgress(id: Int, data: PaymentEntity)
    suspend fun getLocalData(): List<PaymentEntity>
    suspend fun getLocalElement(id: Int): PaymentEntity?
    suspend fun getLastPayment(): PaymentEntity?
    suspend fun clearLocalData()
}