package cl.ruben.paymentapp.data.repository

import cl.ruben.paymentapp.data.mappers.ModelPaymentMappers
import cl.ruben.paymentapp.data.mappers.RoomEntitiesPaymentMappers
import cl.ruben.paymentapp.data.repository.source.local.PaymentMethodsLocalSource
import cl.ruben.paymentapp.data.repository.source.remote.PaymentMethodsRemoteSource
import cl.ruben.paymentapp.domain.entities.*
import cl.ruben.paymentapp.domain.interfaces.PaymentRepositoryInterface
import javax.inject.Inject

class PaymentRepository @Inject constructor(
    private val source: PaymentMethodsRemoteSource,
    private val mapper: ModelPaymentMappers,
    private val daoMapper: RoomEntitiesPaymentMappers,
    private val localSource: PaymentMethodsLocalSource,
) : PaymentRepositoryInterface {

    override suspend fun fetchPaymentMethods(): List<PaymentMethodEntity> {
        return mapper.listPaymentMethodModelToListEntity(source.fetchPaymentMethods())
    }

    override suspend fun fetchBankList(paymentId: String): List<BankEntity> {
        return mapper.listBankOptionModelToListEntity(
            source.fetchCardIssuers(
                paymentId
            )
        )
    }

    override suspend fun fetchInstallments(requestEntity: InstallmentRequestEntity): List<InstallmentEntity> {
        return mapper.listInstallmentModelToListInstallmentEntity(
            source.fetchInstallments(
                mapper.installmentRequestEntityToModel(
                    requestEntity
                )
            )
        )
    }

    override suspend fun createLocal(data: PaymentEntity): Int {
        return localSource.createPayment(daoMapper.parseDataToJson(data))
    }

    override suspend fun updateProgress(id: Int, data: PaymentEntity) {
        localSource.saveProgress(id, daoMapper.parseDataToJson(data))
    }

    override suspend fun getLocalData(): List<PaymentEntity> {
        return daoMapper.listPaymentRoomEntityToEntity(localSource.fetchPayments())
    }

    override suspend fun getLocalElement(id: Int): PaymentEntity? {
        return daoMapper.paymentEntityFromJson(localSource.getPayment(id).data)
    }

    override suspend fun getLastPayment(): PaymentEntity {
        return daoMapper.listPaymentRoomEntityToEntity(localSource.fetchPayments())
            .first { true }
    }

    override suspend fun clearLocalData() {
        return localSource.clearPayments()
    }


}