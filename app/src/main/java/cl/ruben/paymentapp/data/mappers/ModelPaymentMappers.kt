package cl.ruben.paymentapp.data.mappers

import cl.ruben.paymentapp.data.models.*
import cl.ruben.paymentapp.domain.entities.*

class ModelPaymentMappers {
    private fun paymentMethodModelToEntity(model: PaymentMethodModel): PaymentMethodEntity {
        return PaymentMethodEntity(
            model.name,
            model.secure_thumbnail,
            model.id,
            model.min_allowed_amount,
            model.max_allowed_amount,
            model.payment_type_id
        )
    }

    fun listPaymentMethodModelToListEntity(model: List<PaymentMethodModel>): List<PaymentMethodEntity> {
        return model.map { paymentMethodModelToEntity(it) }
    }

    fun listBankOptionModelToListEntity(model: List<BankOptionModel>): List<BankEntity> {
        return model.map { bankOptionModelToEntity(it) }
    }

    private fun bankOptionModelToEntity(model: BankOptionModel): BankEntity {
        return BankEntity(
            model.name,
            model.secure_thumbnail,
            model.id
        )
    }

    private fun installmentOptionsModelToEntity(model: InstallmentModel): InstallmentEntity {
        return InstallmentEntity(
            model.payment_method_id,
            model.payment_type_id,
            issuerModelToBankEntity(model.issuer),
            model.payer_costs.map { installmentModelToEntity(it) }
        )
    }

    private fun installmentModelToEntity(model: InstallmentOptionModel): InstallmentOptionEntity {
        return InstallmentOptionEntity(
            model.payment_method_option_id,
            model.recommended_message,
            model.installments,
            model.installment_amount,
            model.total_amount,
            model.installment_rate,
            model.labels,
        )
    }

    private fun issuerModelToBankEntity(model: IssuerModel): BankEntity {
        return BankEntity(
            model.name,
            model.secure_thumbnail,
            model.id
        )
    }

    fun installmentRequestEntityToModel(entity: InstallmentRequestEntity): InstallmentsRequestModel {
        return InstallmentsRequestModel(
            entity.payment_id,
            entity.amount,
            entity.issuer_id
        )
    }

    fun listInstallmentModelToListInstallmentEntity(model: List<InstallmentModel>): List<InstallmentEntity> {
        return model.map {
            installmentOptionsModelToEntity(it)
        }
    }
}

