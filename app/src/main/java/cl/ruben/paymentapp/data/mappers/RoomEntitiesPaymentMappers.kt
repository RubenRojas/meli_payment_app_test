package cl.ruben.paymentapp.data.mappers

import cl.ruben.paymentapp.data.repository.source.local.room.roomEntities.PaymentRoomEntity
import cl.ruben.paymentapp.domain.entities.PaymentEntity
import com.google.gson.Gson

class RoomEntitiesPaymentMappers {
    fun paymentEntityFromJson(json: String): PaymentEntity? {
        return Gson().fromJson(
            json,
            PaymentEntity::class.java
        )
    }

    fun listPaymentRoomEntityToEntity(list: List<PaymentRoomEntity>): List<PaymentEntity> {
        return list.map { paymentEntityFromJson(it.data)!! }
    }

    fun parseDataToJson(entity: PaymentEntity): String {
        return Gson().toJson(entity)
    }
}