package cl.ruben.paymentapp.data.models

data class PaymentMethodModel(
    val id: String,
    val name: String,
    val payment_type_id: String,
    val status: String,
    val secure_thumbnail: String,
    val thumbnail: String?,
    val deferred_capture: String,
    val settings: List<SettingModel>,
    val additional_info_needed: List<String>,
    val min_allowed_amount: Long,
    val max_allowed_amount: Long,
    val accreditation_time: Long,
    val financial_institutions: List<Any?>,
    val processing_modes: List<String>
)


data class SettingModel(
    val card_number: CardNumberModel,
    val bin: BinModel,
    val security_code: SecurityCodeModel
)

data class BinModel(
    val pattern: String,
    val installments_pattern: String,
    val exclusion_pattern: String? = null
)

data class CardNumberModel(
    val validation: String,
    val length: Long
)


data class SecurityCodeModel(
    val length: Long,
    val card_location: String,
    val mode: String
)




