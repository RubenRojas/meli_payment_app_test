package cl.ruben.paymentapp.presentation.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import cl.ruben.paymentapp.databinding.ItemLayoutInstallmentSelectorBinding
import cl.ruben.paymentapp.presentation.presenters.InstallmentPresenter

class InstallmentSelectorRecyclerViewAdapter(
    private val items: List<InstallmentPresenter>,
    private val listener: (InstallmentPresenter) -> Unit,
) : RecyclerView.Adapter<InstallmentSelectorRecyclerViewAdapter.ViewHolder>() {

    private var selectedPosition = -1

    inner class ViewHolder(binding: ItemLayoutInstallmentSelectorBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val message = binding.installmentMessage
        val labels = binding.installmentLabels
        val radio: RadioButton = binding.installmentItemRadio
        val infoContainer = binding.installmentDataContainer
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            ItemLayoutInstallmentSelectorBinding.inflate(
                LayoutInflater.from(
                    parent.context,
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.radio.isChecked = position == selectedPosition
        holder.message.text = item.message
        holder.infoContainer.setOnClickListener {
            setCheckedItem(item, holder)
        }
        holder.radio.setOnClickListener {
            setCheckedItem(item, holder)
        }

        holder.labels.text = item.formatLabels()

    }

    private fun setCheckedItem(item: InstallmentPresenter, holder: ViewHolder) {
        listener(item)
        val indexCopy = selectedPosition
        selectedPosition = holder.absoluteAdapterPosition

        notifyItemChanged(indexCopy)
        notifyItemChanged(selectedPosition)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}