package cl.ruben.paymentapp.presentation.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import cl.ruben.paymentapp.databinding.FragmentOperationResumeBinding
import cl.ruben.paymentapp.infraestructure.TAG
import cl.ruben.paymentapp.infraestructure.network.CustomStatus
import cl.ruben.paymentapp.presentation.presenters.PaymentPresenter
import cl.ruben.paymentapp.presentation.viewmodels.OperationResumeViewModel
import cl.ruben.paymentapp.presentation.viewmodels.OperationResumeViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class OperationResumeFragment : Fragment() {

    lateinit var binding: FragmentOperationResumeBinding

    @Inject
    lateinit var factory: OperationResumeViewModelFactory
    lateinit var viewModel: OperationResumeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOperationResumeBinding.inflate(
            LayoutInflater.from(
                requireContext()
            ), container, false
        )

        initViewModel()
        initView()
        loadData()
        return binding.root

    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this, factory
        )[OperationResumeViewModel::class.java]

    }

    private fun initView() {
        binding.closeButton.setOnClickListener {
            viewModel.clearLocalPayments().observe(this as LifecycleOwner) {
                val action: NavDirections =
                    OperationResumeFragmentDirections.actionOperationResumeFragmentToAmountInputFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun loadData() {
        viewModel.getLastPayment()
            .observe(this as LifecycleOwner) { response ->
                Log.d(TAG, response.toString())
                run {
                    when (response.status) {
                        CustomStatus.SUCCESS -> {
                            context?.let {
                                response.data?.let { it1 -> drawData(it1) }
                            }

                        }
                        CustomStatus.ERROR -> {
                        }
                        CustomStatus.LOADING -> {

                        }
                    }
                }
            }
    }

    private fun drawData(data: PaymentPresenter) {
        binding.paymentDialogAmount.text = data.amount.toString()
        binding.paymentDialogBankName.text = data.bank?.name
        binding.paymentDialogInstallmentMessage.text = data.installment?.message
        binding.paymentDialogPaymentName.text = data.payment?.name
    }

}