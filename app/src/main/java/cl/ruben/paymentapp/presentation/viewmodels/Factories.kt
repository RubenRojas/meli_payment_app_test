package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cl.ruben.paymentapp.domain.usecases.PaymentUseCase
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import javax.inject.Inject

class PaymentMethodViewModelFactory @Inject constructor(
    private val useCase: PaymentUseCase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PaymentMethodViewModel(useCase, mapper) as T
    }
}

class BankSelectorViewModelFactory @Inject constructor(
    private val useCase: PaymentUseCase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return BankSelectorViewModel(useCase, mapper) as T
    }
}

class InstallmentSelectorViewModelFactory @Inject constructor(
    private val useCase: PaymentUseCase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return InstallmentSelectorViewModel(useCase, mapper) as T
    }
}

class AmountInputViewModelFactory @Inject constructor(
    private val useCase: PaymentUseCase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AmountInputViewModel(useCase, mapper) as T
    }
}

class OperationResumeViewModelFactory @Inject constructor(
    private val useCase: PaymentUseCase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return OperationResumeViewModel(useCase, mapper) as T
    }
}