package cl.ruben.paymentapp.presentation.mappers

import cl.ruben.paymentapp.domain.entities.*
import cl.ruben.paymentapp.presentation.presenters.*

class PresenterPaymentMappers {
    fun paymentMethodEntityToPresenter(entity: PaymentMethodEntity): PaymentMethodPresenter {
        return PaymentMethodPresenter(
            entity.name,
            entity.thumbnail,
            entity.id,
            entity.minAmount,
            entity.maxAmount,
            entity.paymentTypeId,
        )
    }

    fun bankEntityToPresenter(entity: BankEntity): BankPresenter {
        return BankPresenter(
            entity.id,
            entity.name,
            entity.thumbnail
        )
    }

    fun installmentRequestPresenterToEntity(
        presenter: InstallmentRequestPresenter
    ): InstallmentRequestEntity {
        return InstallmentRequestEntity(
            presenter.payment_id,
            presenter.amount,
            presenter.issuer_id
        )
    }

    fun installmentEntityToListInstallmentPresenter(
        entity: InstallmentEntity
    ): List<InstallmentPresenter> {
        return entity.options.map { p ->
            InstallmentPresenter(
                p.id,
                p.message,
                p.installments,
                p.installmentAmount,
                p.totalAmount,
                p.installment_rate,
                p.labels,
            )
        }
    }

    fun amountInputPresenterToPaymentEntity(
        presenter: AmountInputPresenter
    ): PaymentEntity {
        val amount: Int = presenter.toNumber()
        return PaymentEntity(
            null, null, null, amount
        )

    }

    fun bankPresenterToEntity(presenter: BankPresenter): BankEntity {
        return BankEntity(
            presenter.name,
            presenter.thumbnail,
            presenter.id
        )
    }

    fun paymentMethodPresenterToEntity(presenter: PaymentMethodPresenter): PaymentMethodEntity {
        return PaymentMethodEntity(
            presenter.name,
            presenter.thumbnail,
            presenter.id,
            presenter.minAmount,
            presenter.maxAmount,
            presenter.paymentTypeId
        )
    }

    fun installmentPresenterToEntity(p: InstallmentPresenter): InstallmentOptionEntity {
        return InstallmentOptionEntity(
            p.id,
            p.message,
            p.installments,
            p.installmentAmount,
            p.totalAmount,
            p.installment_rate,
            p.labels,
        )
    }

    private fun bankEntityToBankPresenter(b: BankEntity): BankPresenter {
        return BankPresenter(
            b.id,
            b.name,
            b.thumbnail
        )
    }

    private fun paymentEntityToPaymentMethodPresenter(p: PaymentMethodEntity): PaymentMethodPresenter {
        return PaymentMethodPresenter(
            p.name,
            p.thumbnail,
            p.id,
            p.minAmount,
            p.maxAmount,
            p.paymentTypeId,
        )
    }

    private fun installmentOptionEntityToInstallmentPresenter(i: InstallmentOptionEntity): InstallmentPresenter {
        return InstallmentPresenter(
            i.id,
            i.message,
            i.installments,
            i.installmentAmount,
            i.totalAmount,
            i.installment_rate,
            i.labels,
        )
    }

    fun paymentEntityToPresenter(e: PaymentEntity): PaymentPresenter {
        return PaymentPresenter(
            e.bank?.let { this.bankEntityToBankPresenter(it) },
            e.payment?.let { this.paymentEntityToPaymentMethodPresenter(it) },
            e.installment?.let {
                this.installmentOptionEntityToInstallmentPresenter(
                    it
                )
            },
            e.amount,
            e.hasDisplayed,
        )
    }

}