package cl.ruben.paymentapp.presentation.presenters

data class PaymentPresenter(
    val bank: BankPresenter?,
    val payment: PaymentMethodPresenter?,
    val installment: InstallmentPresenter?,
    val amount: Int?,
    val hasDisplayed: Boolean
)

data class BankPresenter(
    val id: String,
    val name: String,
    val thumbnail: String,
)

data class PaymentMethodPresenter(
    val name: String,
    val thumbnail: String,
    val id: String,
    val minAmount: Long,
    val maxAmount: Long,
    val paymentTypeId: String,
)

data class InstallmentPresenter(
    val id: String,
    val message: String,
    val installments: Long, // cuotas
    val installmentAmount: Double, // cantidad_cuotas
    val totalAmount: Double,  // montoTotal
    val installment_rate: Double,
    val labels: List<String>,
) {
    fun formatLabels(): String {
        val label = labels.last();
        val elements = label.split("|")
        if (elements.isNotEmpty()) {
            return elements.joinToString(" ") { it.replace("_", " - ") }
        }
        return ""
    }
}

data class InstallmentRequestPresenter(
    val payment_id: String,
    val amount: Int,
    val issuer_id: Int,
)