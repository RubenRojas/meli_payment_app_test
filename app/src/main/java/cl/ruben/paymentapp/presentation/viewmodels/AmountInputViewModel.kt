package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.paymentapp.PaymentApp
import cl.ruben.paymentapp.domain.usecases.PaymentUseCase
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import cl.ruben.paymentapp.presentation.presenters.AmountInputPresenter
import kotlinx.coroutines.Dispatchers

class AmountInputViewModel(
    private val useCase: PaymentUseCase,
    private val mapper: PresenterPaymentMappers,
) : ViewModel() {

    fun saveAmount(presenter: AmountInputPresenter) = liveData(Dispatchers.IO) {
        PaymentApp.currentId = useCase.initLocalPaymentData(
            mapper.amountInputPresenterToPaymentEntity(presenter)
        )
        emit(true)
    }

    fun validateInput(text: String): Boolean {
        return text.isNotBlank() && text.isNotEmpty()
    }

}