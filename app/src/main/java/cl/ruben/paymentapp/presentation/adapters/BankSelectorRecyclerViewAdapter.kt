package cl.ruben.paymentapp.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import cl.ruben.paymentapp.databinding.ItemLayoutBankSelectorBinding
import cl.ruben.paymentapp.presentation.presenters.BankPresenter
import com.squareup.picasso.Picasso

class BankSelectorRecyclerViewAdapter(
    private val items: List<BankPresenter>,
    private val listener: (BankPresenter) -> Unit,
) : RecyclerView.Adapter<BankSelectorRecyclerViewAdapter.ViewHolder>() {

    private var selectedPosition = -1

    inner class ViewHolder(binding: ItemLayoutBankSelectorBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val itemName = binding.bankItemName
        val itemImage = binding.bankItemImage
        val radio: RadioButton = binding.bankItemRadio
        val container = binding.bankItemContainer
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            ItemLayoutBankSelectorBinding.inflate(
                LayoutInflater.from(
                    parent.context,
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.radio.isChecked = position == selectedPosition
        holder.itemName.text = item.name
        holder.itemName.setOnClickListener {
            setCheckedItem(item, holder)
        }
        holder.radio.setOnClickListener {
            setCheckedItem(item, holder)
        }
        Picasso.get().load(item.thumbnail).into(holder.itemImage)
    }

    private fun setCheckedItem(item: BankPresenter, holder: ViewHolder) {
        listener(item)
        val indexCopy = selectedPosition
        selectedPosition = holder.absoluteAdapterPosition

        notifyItemChanged(indexCopy)
        notifyItemChanged(selectedPosition)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}