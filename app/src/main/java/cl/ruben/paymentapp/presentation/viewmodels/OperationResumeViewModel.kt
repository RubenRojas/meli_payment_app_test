package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.paymentapp.domain.usecases.PaymentUseCase
import cl.ruben.paymentapp.infraestructure.default_error_message
import cl.ruben.paymentapp.infraestructure.network.CustomResponse
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import kotlinx.coroutines.Dispatchers

class OperationResumeViewModel(
    private val useCase: PaymentUseCase,
    private val mapper: PresenterPaymentMappers,
) : ViewModel() {

    fun getLastPayment() = liveData(Dispatchers.IO) {
        try {
            val entity = useCase.getLastPayment()
            emit(
                CustomResponse.success(
                    mapper.paymentEntityToPresenter(
                        entity
                    )
                )
            )
        } catch (e: Exception) {
            emit(CustomResponse.error(e.message ?: default_error_message))
        }
    }

    fun clearLocalPayments() = liveData(Dispatchers.IO) {
        try {
            useCase.clearLocalData()

        } catch (_: Exception) {

        }
        emit(true)
    }
}