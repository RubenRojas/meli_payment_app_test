package cl.ruben.paymentapp.infraestructure.network

enum class CustomStatus {
    SUCCESS,
    ERROR,
    LOADING,
}

data class CustomResponse<out T>(val status: CustomStatus, val data: T?) {
    companion object {
        fun <T> success(data: T): CustomResponse<T> =
            CustomResponse(status = CustomStatus.SUCCESS, data = data)

        fun <T> error(message: String): CustomResponse<T> =
            CustomResponse(status = CustomStatus.ERROR, data = message as T)

        fun <T> loading(): CustomResponse<T> =
            CustomResponse(status = CustomStatus.LOADING, data = null)
    }
}