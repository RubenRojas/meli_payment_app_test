package cl.ruben.paymentapp.infraestructure

const val baseUrl = "https://api.mercadopago.com/"
const val default_error_message = "Sorry, an error happen"
const val requiredPaymentTypeId = "credit_card"
const val TAG = "PaymentAppLog"
const val databaseVersion = 1