package cl.ruben.paymentapp.infraestructure.room

import android.content.Context
import androidx.room.Room
import cl.ruben.paymentapp.data.repository.source.local.room.AppDatabase
import cl.ruben.paymentapp.data.repository.source.local.room.dao.PaymentDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module

class RoomModule {
    @Provides
    fun providePaymentDao(appDatabase: AppDatabase): PaymentDao {
        return appDatabase.PaymentDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "PaymentAppDatabase"
        ).fallbackToDestructiveMigration()
            .build()
    }
}